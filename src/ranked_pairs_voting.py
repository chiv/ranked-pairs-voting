'''Requires Python 3.9+
'''

from dataclasses import dataclass
from collections import Counter
import networkx as nx
import random

@dataclass
class Ballot:
    ranking: list[str]

@dataclass
class Pair:
    winner: str
    winner_votes: int
    non_winner: str
    non_winner_votes: int

    def __repr__(self) -> str:
        return f'{self.winner} ({self.winner_votes}) > {self.non_winner} ({self.non_winner_votes})'

def get_pairs(ballots: list[Ballot], info_context='') -> list[Pair]:
    '''
    Get the pairs of candidates and their votes.
    '''
    candidates = set(ballots[0].ranking)
    assert all(set(ballot.ranking) == candidates for ballot in ballots), 'All ballots must have the same set of candidates' + (' ' + info_context) if info_context else ''

    table = Counter()
    for ballot in ballots:
        for i, ballot_winner in enumerate(ballot.ranking):
            for ballot_non_winner in ballot.ranking[i+1:]:
                table[(ballot_winner, ballot_non_winner)] += 1
    pairs = []
    candidates = list(candidates)
    for i, candidate1 in enumerate(candidates):
        for candidate2 in candidates[i+1:]:
            if table[(candidate1, candidate2)] > table[(candidate2, candidate1)]:
                pairs.append(Pair(candidate1, table[(candidate1, candidate2)], candidate2, table[(candidate2, candidate1)]))
            elif table[(candidate1, candidate2)] < table[(candidate2, candidate1)]:
                pairs.append(Pair(candidate2, table[(candidate2, candidate1)], candidate1, table[(candidate1, candidate2)]))
            else:
                pairs.append(Pair(candidate1, table[(candidate1, candidate2)], candidate2, table[(candidate2, candidate1)]))
                print(f'[Warning] ⚠ Tie between {candidate1} and {candidate2}' + (' ' + info_context) if info_context else '')
    return pairs

def sort_pairs(pairs: list[Pair], ballots: list[Ballot]) -> list[Pair]:
    '''
    ballots: list[Ballot] for tie-breaking

    Sort the pairs by the margin of victory.
    '''
    candidates = set(ballots[0].ranking)
    assert all(set(ballot.ranking) == candidates for ballot in ballots), 'All ballots must have the same set of candidates'
    candidates_from_pairs = set(pair.winner for pair in pairs) | set(pair.non_winner for pair in pairs)
    assert candidates == candidates_from_pairs, 'Pairs must have the same set of candidates as the ballots'

    pairs = sorted(pairs, key=lambda pair: pair.winner_votes - pair.non_winner_votes, reverse=True)

    # tie-breaking
    # group pairs with the same margin of victory
    pairs_group = [[pairs[0]]]
    for pair in pairs[1:]:
        if pair.winner_votes - pair.non_winner_votes == pairs_group[-1][0].winner_votes - pairs_group[-1][0].non_winner_votes:
            pairs_group[-1].append(pair)
        else:
            pairs_group.append([pair])
    # tie-breaking within each group
    # add pairs whose non-winner exists in previous pairs' non-winner first
    # tie-breaking using a random ballot adding the pair with the non-winner ranked lower
    # if the non-winners are the same in the pairs, sort by the winner ranked higher
    pairs = []
    non_winners = set()
    for group in pairs_group:
        while group:
            group1 = [pair for pair in group if pair.non_winner in non_winners] # non-winners in previous pairs' non-winner
            if len(group1) > 0:
                ballot = random.choice(ballots)
                pair = max(group1, key=lambda pair: (ballot.ranking.index(pair.non_winner), -ballot.ranking.index(pair.winner)))
                group1.remove(pair)
            else:
                ballot = random.choice(ballots)
                pair = max(group, key=lambda pair: (ballot.ranking.index(pair.non_winner), -ballot.ranking.index(pair.winner)))
            pairs.append(pair)
            group.remove(pair)
            non_winners.add(pair.non_winner)
    return pairs

def build_lock_graph(pairs: list[Pair]) -> nx.DiGraph:
    '''
    Build the lock graph.
    '''
    G = nx.DiGraph()
    for pair in pairs:
        if pair.winner in G and pair.non_winner_votes in nx.ancestors(G, pair.winner):
            continue
        G.add_edge(pair.winner, pair.non_winner)
    return G

def get_winner_from_graph(graph: nx.DiGraph) -> str:
    '''
    Get the winner from the graph.
    '''
    winner_list = [node for node, out_degree in graph.in_degree() if out_degree == 0]
    if len(winner_list) != 1:
        return str(winner_list)
    return winner_list[0] # type: ignore
