import argparse
from openpyxl import load_workbook
from ranked_pairs_voting import Ballot, Pair, get_pairs, sort_pairs, build_lock_graph, get_winner_from_graph
from rich.console import Console
from rich.table import Table
import networkx as nx
import sys

def main(args):
    worksheet_data = get_worksheet_data(args.filename, args.sheet)
    do_ranking_results(worksheet_data)

def do_ranking_results(worksheet_data):
    ballot_columns = guess_ranking_column_indices(worksheet_data)

    for i_col in ballot_columns: # for each role voted on
        role = worksheet_data[0][i_col]
        print(f'========== Position: {role} ==========')
        column_data = [row[i_col] for row in worksheet_data]
        ballots = get_ballots(column_data[1:], f'{role}')
        pairs = get_pairs(ballots)
        pairs = sort_pairs(pairs, ballots)
        print_pairs(pairs)
        lock_graph = build_lock_graph(pairs)
        nx.write_network_text(lock_graph, ascii_only=True) # type:ignore
        winner = get_winner_from_graph(lock_graph)
        print(f'🎉 {winner} elected as {role} 🎉')
        print()

def get_ballots(ballots_data, context_info='') -> list[Ballot]:
    '''
    Get ballots given ranking column (excluding title row)
    Filter out ballots does not include all candidates for each role

    args:
        ballots_data: list of column cells excluding title row (first row)
    '''
    if context_info:
        context_info = f'[{context_info}]'

    # get all candidates
    candidates = set()
    for cell in ballots_data:
        if cell is not None:
            candidates |= set(cell[:-1].split(';'))

    num_abstain = 0
    num_invalid = 0
    ballots = []
    for cell in ballots_data:
        if cell is None:
            num_abstain += 1
            continue
        ballot_ = cell[:-1].split(';')
        if set(ballot_) != candidates:
            num_invalid += 1
            continue
        ballots.append(Ballot(ballot_))
    if num_invalid > 0:
        print(f'[Warning] ⚠ {num_invalid} votes did not include all candidates {context_info}', file=sys.stderr)
    print(f'[INFO] {num_abstain} abstains, {len(ballots)} votes {context_info}', file=sys.stderr)
    return ballots

def print_pairs(pairs: list[Pair]):
    console = Console()
    table = Table('Winner', 'Winner Votes', 'Non-winner', 'Non-winner Votes', 'Margin')
    for pair in pairs:
        table.add_row(pair.winner, str(pair.winner_votes), pair.non_winner, str(pair.non_winner_votes), str(pair.winner_votes - pair.non_winner_votes))
    console.print(table)

def get_worksheet_data(workbook_filename, worksheet_name):
    wb = load_workbook(workbook_filename)
    return list(wb[worksheet_name].values)

def guess_ranking_column_indices(worksheet_data: list[tuple]) -> list[int]:
    '''
    Guess the list of column in the input table which are ranking questions, and return their indices
    A column is of ranking questions (e.g. not for metadata or other questions in the form) if all cells in the column are rankings (except for the first row being e.g. the name of the role):
    - All non-empty cells in the column end with a semicolon
    - Cells in the column can be empty (e.g. abstain)
    '''
    rank_columns = []

    # for each column
    for i in range(len(worksheet_data[0])):
        skip_column = False
        candidates = set()

        # for each row, except title row
        for j in range(1, len(worksheet_data)):
            # skip empty rows
            if worksheet_data[j][i] is None or worksheet_data[j][i] == '':
                continue

            if type(worksheet_data[j][i]) != str:
                skip_column = True
                break

            if worksheet_data[j][i][-1] != ';':
                skip_column = True
                break

            cell_options = worksheet_data[j][i][:-1].split(';')
            candidates |= set(cell_options)
        if not skip_column and len(candidates) > 1: # at least 2 candidates to consider as ranking
            rank_columns.append(i)
    return rank_columns

if __name__ == '__main__':
    '''
    Data assumption:
    - Candidate names does not contain semicolon
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str, help='Result Excel file from Microsoft Forms')
    parser.add_argument('--sheet', type=str, default='Sheet1', help='Sheet name')
    args = parser.parse_args()
    main(args)
