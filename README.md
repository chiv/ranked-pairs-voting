# Ranked Pairs Voting Stuff

Do Ranked Pairs Voting on ballots collected with Microsoft Forms.

## Requirements

- Python 3.9 or newer

## Example Output

```plaintext
========== Position: Favourite Sauce ==========
┏━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━┳━━━━━━━━┓
┃ Winner          ┃ Winner Votes ┃ Non-winner   ┃ Non-winner Votes ┃ Margin ┃
┡━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━╇━━━━━━━━┩
│ English mustard │ 9            │ Ketchup      │ 2                │ 7      │
│ HP Sauce        │ 8            │ Ketchup      │ 3                │ 5      │
│ HP Sauce        │ 8            │ None of them │ 3                │ 5      │
│ English mustard │ 8            │ None of them │ 3                │ 5      │
│ None of them    │ 7            │ Ketchup      │ 4                │ 3      │
│ Mayonnaise      │ 7            │ Ketchup      │ 4                │ 3      │
│ HP Sauce        │ 7            │ Mayonnaise   │ 4                │ 3      │
│ English mustard │ 7            │ Mayonnaise   │ 4                │ 3      │
│ Mayonnaise      │ 6            │ None of them │ 5                │ 1      │
│ English mustard │ 6            │ HP Sauce     │ 5                │ 1      │
└─────────────────┴──────────────┴──────────────┴──────────────────┴────────┘
╙── English mustard
    ├─╼ Ketchup ╾ HP Sauce, None of them, Mayonnaise
    ├─╼ None of them ╾ HP Sauce, Mayonnaise
    │   └─╼  ...
    ├─╼ Mayonnaise ╾ HP Sauce
    │   └─╼  ...
    └─╼ HP Sauce
        └─╼  ...
🎉 English mustard elected as Favourite Sauce 🎉
```
